package com.neelam.surveysampleapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.neelam.surveysdk.NKSurvey;


/**
 * Created by Neelam on 8/28/2016.
 */
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        //register  with survey sdk
        NKSurvey.launchSurveySDK(this, getIntent().getExtras());

    }


}
